package leopoldino.gabriel.rasterization

import java.awt.Color
import java.awt.GridLayout
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import javax.swing.*

/**
 * Created by gabriel on 03/07/17.
 */


fun main(args: Array<String>) {

    val gui = GUI()
    gui.isVisible = true
    print("Visible")
}

class PixelButton : JButton
{
    val point : Point2D

    constructor(point : Point2D)
    {
        this.point = point
    }
}
enum class Opcao
{
    RETA, CIRCULO, ELIPSE
}

class GUI : JFrame{

    val matTam = 30
    var opcao : Opcao = Opcao.RETA
    var originPoint : Point2D? = null
    lateinit var buttons : Array<Array<PixelButton?>>
    lateinit var algReta : AlgoritmoReta
    val console = JEditorPane()
    val slRaio = JSlider()
    val slExMaior = JSlider()
    val slExMenor = JSlider()
    val backgroundColor = Color.WHITE
    val checkedColor = Color.BLACK

    var buttonsListener: ActionListener = object : AbstractAction() {
        override fun actionPerformed(e: ActionEvent) {
            val bt = e.source as PixelButton
            when (opcao) {
                Opcao.RETA -> {
                    if (originPoint == null)
                        originPoint = bt.point
                    else {
                        val lastPoint = bt.point
                        console.text += "Inicio $originPoint \n"
                        console.text += "Final $lastPoint \n"

                        for (p in algReta.desenhar(originPoint!!, lastPoint)) {
                            buttons[p.x][p.y]?.background= checkedColor
                            console.text += p.toString()
                        }
                        originPoint = null
                    }
                }
                Opcao.CIRCULO -> {
                    for (p in Circulo.desenhar(bt.point, slRaio.value)){
                        if (p.x >= 0 && p.x <= matTam-1 && p.y >= 0 &&p.y <= matTam-1)
                        {
                            buttons[p.x][p.y]?.background= checkedColor

                        }
                    }
                }
                Opcao.ELIPSE ->
                {
                    for (p in Elipse.desenhar(bt.point, slExMaior.value, slExMenor.value)){
                        if (p.x >= 0 && p.x <= matTam-1 && p.y >= 0 &&p.y <= matTam-1)
                        {
                            buttons[p.x][p.y]?.background= checkedColor
                        }
                    }
                }
            }
        }
    }

    constructor() {
        title = "Rasterization"
        defaultCloseOperation = javax.swing.WindowConstants.EXIT_ON_CLOSE
        contentPane.layout = GridLayout(1, 2)
        setSize(800, 600)
        val gridPanel = JPanel()
        gridPanel.layout = GridLayout(matTam, matTam)
        buttons = Array(matTam) { arrayOfNulls<PixelButton>(matTam) }
        for (i in 0..matTam-1)
        {
            for (j in 0..matTam-1)
            {
                buttons[i][j] = PixelButton(Point2D(i, j))
                buttons[i][j]?.background= backgroundColor
                buttons[i][j]?.addActionListener(buttonsListener)
                gridPanel.add(buttons[i][j])


            }
        }

        val setupPanel = JPanel(GridLayout(5,1))
        val retaPanel = JPanel(GridLayout(3, 1))

        val grupoRetas = ButtonGroup()
        val retaBresenhan = JRadioButton("Bresenham")
        retaBresenhan.addActionListener { algReta = Bresenhan
        opcao = Opcao.RETA}
        val retaDDA = JRadioButton("DDA")
        retaDDA.addActionListener { algReta = DDA
        opcao = Opcao.RETA}
        val retaAnalitico = JRadioButton("Método Analítico")
        retaAnalitico.addActionListener { algReta = Analitico
        opcao = Opcao.RETA}

        val circuloPanel = JPanel(GridLayout(2, 1))
        val rbCirculo = JRadioButton("Circulo")
        rbCirculo.addActionListener { opcao = Opcao.CIRCULO }
        slRaio.minimum = 1
        slRaio.maximum = matTam
        slRaio.value = 1
        val lbRaio = JLabel("1")
        slRaio.addChangeListener { lbRaio.text = slRaio.value.toString() }

        val panelSlider = JPanel(GridLayout(1, 2))
        panelSlider.add(slRaio)
        panelSlider.add(lbRaio)

        circuloPanel.add(rbCirculo)
        circuloPanel.add(panelSlider)

        val elipsePanel = JPanel(GridLayout(2, 1))
        val rbElipse = JRadioButton("Elipse")
        rbElipse.addActionListener { opcao = Opcao.ELIPSE }
        val panelSlidersElipse = JPanel(GridLayout(2, 2))
        slExMaior.minimum = 1
        slExMaior.maximum = matTam
        slExMaior.value = 1

        slExMenor.minimum = 1
        slExMenor.maximum = matTam
        slExMenor.value = 1

        val lbExMaior = JLabel("1")
        val lbExMenor = JLabel("1")

        slExMaior.addChangeListener { lbExMaior.text = slExMaior.value.toString() }
        slExMenor.addChangeListener { lbExMenor.text = slExMenor.value.toString() }
        panelSlidersElipse.add(slExMaior)
        panelSlidersElipse.add(lbExMaior)
        panelSlidersElipse.add(slExMenor)
        panelSlidersElipse.add(lbExMenor)


        elipsePanel.add(rbElipse)
        elipsePanel.add(panelSlidersElipse)



        grupoRetas.add(retaBresenhan)
        grupoRetas.add(retaDDA)
        grupoRetas.add(retaAnalitico)
        grupoRetas.add(rbCirculo)
        grupoRetas.add(rbElipse)


        retaPanel.add(retaBresenhan)
        retaPanel.add(retaDDA)
        retaPanel.add(retaAnalitico)

        val btClear = JButton("Limpar")
        btClear.addActionListener(ActionListener {
            for (i in 0..matTam-1)
            {
                for (j in 0..matTam-1)
                {
                    buttons[i][j]?.background= backgroundColor

                }
            }
            console.text = ""
            originPoint = null
        })


        setupPanel.add(retaPanel)
        setupPanel.add(circuloPanel)
        setupPanel.add(elipsePanel)
        setupPanel.add(btClear)
        setupPanel.add(console)

        contentPane.add(gridPanel)
        contentPane.add(setupPanel)
    }
}