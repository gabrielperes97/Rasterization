package leopoldino.gabriel.rasterization

/**
 * Created by gabriel on 03/07/17.
 */
object Circulo
{
    fun circlePixel(point : Point2D, origem : Point2D):List<Point2D>
    {
        val pixels = ArrayList<Point2D>()
        pixels.add(Point2D(point.x+origem.x, point.y+origem.y))
        pixels.add(Point2D(point.y+origem.x, point.x+origem.y))
        pixels.add(Point2D(point.y+origem.x, -point.x+origem.y))
        pixels.add(Point2D(point.x+origem.x, -point.y+origem.y))
        pixels.add(Point2D(-point.x+origem.x, -point.y+origem.y))
        pixels.add(Point2D(-point.y+origem.x, -point.x+origem.y))
        pixels.add(Point2D(-point.y+origem.x, point.x+origem.y))
        pixels.add(Point2D(-point.x+origem.x, point.y+origem.y))
        return pixels
    }

    fun desenhar(origem:Point2D, raio : Int):List<Point2D>
    {
        val pixels = ArrayList<Point2D>()
        var x=0
        var y=raio
        var d = 1-raio

        pixels.addAll(circlePixel(Point2D(x, y), origem))
        while (y > x)
        {
            if (d < 0)
            {
                d += 2 * x + 3
            }else
            {
                d += 2 * (x-y) + 5
                --y
            }
            ++x
            pixels.addAll(circlePixel(Point2D(x, y), origem))
        }
        return pixels;

    }
}