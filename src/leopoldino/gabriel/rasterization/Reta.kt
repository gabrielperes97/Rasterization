package leopoldino.gabriel.rasterization

/**
 * Created by gabriel on 02/07/17.
 */

interface AlgoritmoReta{
    fun desenhar(inicio: Point2D, fim: Point2D):List<Point2D>
}

object Bresenhan : AlgoritmoReta{
    override fun desenhar(inicio: Point2D, fim: Point2D):List<Point2D> {
        val pixels = ArrayList<Point2D>()

        var dx = fim.x - inicio.x
        var dy = fim.y - inicio.y

        if (dx < 0) dx = -dx
        if (dy < 0) dy = -dy
        var incx = 1
        if (fim.x < inicio.x) incx = -1
        var incy = 1
        if (fim.y < inicio.y) incy = -1
        var x = inicio.x
        var y = inicio.y

        if (dx > dy)
        {
            pixels.add(Point2D(x, y))
            var e = 2*dy - dx
            var inc1 = 2*( dy -dx);
            var inc2 = 2*dy;
            for (i in 0..dx-1) //talvez é dx-1
            {
                if (e >= 0)
                {
                    y += incy
                    e += inc1
                }
                else
                    e += inc2
                x += incx
                pixels.add(Point2D(x, y))
            }
        }
        else
        {
            pixels.add(Point2D(x, y))
            var e = 2*dx - dy;
            var inc1 = 2*( dx - dy);
            var inc2 = 2*dx;
            for (i in 0..dy-1) //talvez é dy-1
            {
                if (e >= 0)
                {
                    x += incx
                    e += inc1
                }
                else
                    e += inc2
                y += incy
                pixels.add(Point2D(x, y))
            }
        }
        return pixels;
    }
}

object DDA : AlgoritmoReta
{
    override fun desenhar(inicio: Point2D, fim: Point2D): List<Point2D> {
        val pixels = ArrayList<Point2D>()
        var dx = fim.x - inicio.x
        var dy = fim.y - inicio.y
        var step:Int
        if (Math.abs(dx) > Math.abs(dy))
            step = Math.abs(dx)
        else
            step = Math.abs(dy)
        var incx = dx.toDouble()/step.toDouble()
        var incy = dy.toDouble()/step.toDouble()
        var x : Double = inicio.x.toDouble()
        var y : Double = inicio.y.toDouble()
        pixels.add(Point2D(Math.round(x).toInt(), Math.round(y).toInt()))
        for (i in 1..step)
        {
            x += incx
            y += incy
            pixels.add(Point2D(Math.round(x).toInt(), Math.round(y).toInt()))
        }
        return pixels;
    }
}

object Analitico : AlgoritmoReta{
    override fun desenhar(inicio: Point2D, fim: Point2D): List<Point2D> {
        val pixels = ArrayList<Point2D>()
        // 7 < 0
        if (fim.y < inicio.y)
            return desenhar(fim, inicio)
        if (inicio.x != fim.x) {
            val m = (fim.y - inicio.y).toDouble() / (fim.x - inicio.x).toDouble()
            val b = fim.y - m * fim.x
            if (inicio.x < fim.x)
                for (x in inicio.x..fim.x) {
                    var y = m * x + b
                    pixels.add(Point2D(x, y.toInt()))
                }
            else
                for (x in fim.x..inicio.x) {
                    var y = m * x + b
                    pixels.add(Point2D(x, y.toInt()))
                }
        }else
        {
            for (y in inicio.y..fim.y)
                pixels.add(Point2D(inicio.x, y))
        }
        return pixels
    }


}