package leopoldino.gabriel.rasterization

/**
 * Created by gabriel on 03/07/17.
 */

object Elipse{

    fun elipticPixel(point : Point2D, origem : Point2D):List<Point2D>
    {
        val pixels = ArrayList<Point2D>(2)
        pixels.add(Point2D(point.x+origem.x, point.y+origem.y))
        pixels.add(Point2D(-point.x+origem.x, point.y+origem.y))
        pixels.add(Point2D(-point.x+origem.x, -point.y+origem.y))
        pixels.add(Point2D(point.x+origem.x, -point.y+origem.y))

        return pixels
    }

    fun desenhar(origem:Point2D, eixoMaior:Int, eixoMenor:Int):List<Point2D>
    {
        val pixels = ArrayList<Point2D>()

        for (i in 0..90)
        {
            var x = eixoMaior*Math.cos(Math.toRadians(i.toDouble()))
            var y = eixoMenor*Math.sin(Math.toRadians(i.toDouble()))
            pixels.addAll(elipticPixel(Point2D(Math.round(x).toInt(), Math.round(y).toInt()), origem))

        }
        //escrever aqui

        return pixels
    }
}