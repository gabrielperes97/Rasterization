package leopoldino.gabriel.rasterization

/**
 * Created by gabriel on 02/07/17.
 */
class Point2D {
    var x = 0
    var y = 0

    constructor() {}

    constructor(x: Int, y: Int)
    {
        this.x = x;
        this.y = y;
    }

    override fun toString(): String {
        return "($x, $y)"
    }


}